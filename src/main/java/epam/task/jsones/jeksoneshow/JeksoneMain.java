package epam.task.jsones.jeksoneshow;


import epam.task.jsones.jsoneshow.JsoneMain;
import epam.task.jsones.model.BankClient;
import org.codehaus.jackson.map.ObjectMapper;

import java.io.File;
import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Paths;


public class JeksoneMain {

    public JeksoneMain() {
        readValue();
    }

    private void readValue() {
        final String JSON_FILE = "banks.json";

        ClassLoader classLoader = JsoneMain.class.getClassLoader();
        File inputFile = new File(classLoader.getResource(JSON_FILE).getFile());

        //read json file data to String
        byte[] jsonData = new byte[0];
        try {
            jsonData = Files.readAllBytes(Paths.get(String.valueOf(inputFile)));
            //create ObjectMapper instance
            ObjectMapper objectMapper = new ObjectMapper();

            //convert json string to object
            BankClient[] clients = objectMapper.readValue(jsonData, BankClient[].class);

            for(BankClient bankClient : clients) {
                System.out.println(bankClient);
            }
        } catch (IOException e) {
            e.printStackTrace();
        }


    }
}