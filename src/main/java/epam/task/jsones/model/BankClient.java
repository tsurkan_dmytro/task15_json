package epam.task.jsones.model;

import java.util.Arrays;

public class BankClient implements Comparable<BankClient> {

    private int id;
    private String depositorName;
    private String bankName;
    private Address address;
    private long[] phoneNumbers;
    private String type;

    private long accountId;
    private int amountOnDeposit;
    private String profitability;
    private String timeConstraints;

    public BankClient() {
    }

    public String getDepositorName() {
        return depositorName;
    }

    public void setDepositorName(String depositorName) {
        this.depositorName = depositorName;
    }

    public long getAccountId() {
        return accountId;
    }

    public void setAccountId(long accountId) {
        this.accountId = accountId;
    }

    public int getAmountOnDeposit() {
        return amountOnDeposit;
    }

    public void setAmountOnDeposit(int amountOnDeposit) {
        this.amountOnDeposit = amountOnDeposit;
    }

    public String getProfitability() {
        return profitability;
    }

    public void setProfitability(String profitability) {
        this.profitability = profitability;
    }

    public String getTimeConstraints() {
        return timeConstraints;
    }

    public void setTimeConstraints(String timeConstraints) {
        this.timeConstraints = timeConstraints;
    }

    public int getId() {
        return id;
    }
    public void setId(int id) {
        this.id = id;
    }

    public String getBankName() {
        return bankName;
    }
    public void setBankName(String bankName) {
        this.bankName = bankName;
    }

    public Address getAddress() {
        return address;
    }
    public void setAddress(Address address) {
        this.address = address;
    }
    public long[] getPhoneNumbers() {
        return phoneNumbers;
    }
    public void setPhoneNumbers(long[] phoneNumbers) {
        this.phoneNumbers = phoneNumbers;
    }
    public String getType() {
        return type;
    }

    public void setType(String type) {
        this.type = type;
    }

    @Override
    public String toString(){
        StringBuilder sb = new StringBuilder();
        sb.append("***** Bank Details *****\n");
        sb.append("ID = "+getId()+"  ");
        sb.append("DepositorName = "+getDepositorName()+"  ");
        sb.append("Name = "+ getBankName()+"  ");
        sb.append("accountId = "+getAccountId()+"  ");
        sb.append("Type = "+getType()+"  ");
        sb.append("Phone Numbers = "+ Arrays.toString(getPhoneNumbers())+"  ");
        sb.append("Country = "+getAddress().getCountry()+"\n");
        sb.append("City = "+getAddress().getCity()+"  ");
        sb.append("ZipCode = "+getAddress().getZipcode()+"  ");
        sb.append("amountOnDeposit = "+getAmountOnDeposit()+"  ");
        sb.append("Profitability = "+getProfitability()+"  ");
        sb.append("TimeConstraints = "+getTimeConstraints()+"  ");
        sb.append("\n*****************************");
        return sb.toString();
    }

    @Override
    public int compareTo(BankClient o) {
        int result = this.depositorName.compareTo(o.depositorName);
        if (result == 0) {
            result = this.bankName.compareTo(o.bankName);
        }
        return result;
    }
}