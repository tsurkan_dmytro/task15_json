package epam.task.jsones.jsoneshow;

import com.google.gson.Gson;
import epam.task.jsones.model.BankClient;

import java.io.*;
import java.util.Objects;

public class JsoneMain {

    public static final String JSON_FILE = "banks.json";

    public JsoneMain() {
        ClassLoader classLoader = JsoneMain.class.getClassLoader();
        File inputFile = new File(Objects.requireNonNull(classLoader.getResource(JSON_FILE)).getFile());
        Gson gson = new Gson();

        try (Reader reader = new FileReader(inputFile)) {

            // Convert JSON File to Java Object
            BankClient[] bankClients = gson.fromJson(reader, BankClient[].class);

            for(BankClient bankClient : bankClients) {
                System.out.println(bankClient);
            }

        } catch (IOException e) {
            e.printStackTrace();
        }
    }

}
