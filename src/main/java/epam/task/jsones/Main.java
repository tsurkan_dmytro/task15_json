package epam.task.jsones;

import com.github.fge.jsonschema.core.exceptions.ProcessingException;
import epam.task.jsones.jeksoneshow.JeksoneMain;
import epam.task.jsones.jsoneshow.JsoneMain;
import epam.task.jsones.utills.ValidationUtils;

import java.io.*;

public class Main {
    public static void main(String[] args) {

        validation();

        JsoneMain jsoneMain = new JsoneMain();
        JeksoneMain jeksoneMain = new JeksoneMain();
    }

    public static void validation()  {
        final String JSON_FILE = "src/main/resources/banks.json";
        final String JSON_CSHEMA = "src/main/resources/schema.json";

        File schemaFile = new File(JSON_CSHEMA);
        File jsonFile = new File(JSON_FILE);

        try {
            if (ValidationUtils.isJsonValid(schemaFile, jsonFile)){
                System.out.println("Valid!");
            }else{
                System.out.println("NOT valid!");
            }
        } catch (ProcessingException e) {
            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
        }
    }
}
